#!/bin/bash

# Build the MySQL Docker image
docker build -t mysql-image .

# Run the MySQL container
docker run -d --name mysql-dev -p 3306:3306 mysql-image

