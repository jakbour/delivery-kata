package com.jakbour.delivery.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import com.jakbour.delivery.entities.DeliveryMode;
import com.jakbour.delivery.entities.Reservation;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ReservationRepositoryTest {

    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    void testSaveAndFindById() {

        // Given
        Reservation reservation = new Reservation(1L, DeliveryMode.DRIVE, LocalDate.now(), LocalTime.of(10, 00),
                LocalTime.of(12, 00), 1L);

        // When
        Reservation savedReservation = reservationRepository.save(reservation);
        Reservation foundReservation = reservationRepository.findById(savedReservation.getId()).orElse(null);

        // Then
        assertThat(foundReservation).isNotNull();
        assertThat(foundReservation.getTimeSlotStart()).isEqualTo(LocalTime.of(10, 00));

    }

    @Test
    void testDeleteReservation() {

        // Given
        Reservation reservation = new Reservation(2L, DeliveryMode.DRIVE, LocalDate.now(), LocalTime.of(10, 00),
                LocalTime.of(12, 00), 1L);
        Reservation savedReservation = reservationRepository.save(reservation);

        // When
        reservationRepository.delete(reservation);

        // Then
        assertThat(savedReservation.getId()).isNotNull();
        assertThat(reservationRepository.findById(savedReservation.getId())).isEmpty();
    }
}
