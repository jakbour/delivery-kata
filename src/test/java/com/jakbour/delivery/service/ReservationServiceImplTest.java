package com.jakbour.delivery.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.jakbour.delivery.entities.DeliveryMode;
import com.jakbour.delivery.entities.Reservation;
import com.jakbour.delivery.repository.ReservationRepository;

@SpringBootTest
class ReservationServiceImplTest {

    @Mock
    private ReservationRepository reservationRepository;

    @InjectMocks
    private ReservationServiceImpl reservationService;

    @Test
    void testDeleteReservation() {
        // Given
        Long reservationId = 1L;

        // When
        reservationService.deleteReservation(reservationId);

        // Then
        verify(reservationRepository, times(1)).deleteById(reservationId);
    }

    @Test
    void testGetAllReservations() {

        // Given
        Reservation reservation1 = new Reservation(1L, DeliveryMode.DRIVE, LocalDate.now(), LocalTime.of(10, 00),
                LocalTime.of(12, 00), 1L);
        Reservation reservation2 = new Reservation(2L, DeliveryMode.DRIVE, LocalDate.now(), LocalTime.of(10, 00),
                LocalTime.of(12, 00), 1L);

        when(reservationRepository.findAll()).thenReturn(Arrays.asList(reservation1, reservation2));

        // When
        List<Reservation> reservations = reservationService.getAllReservations();

        // Then
        assertThat(reservations).hasSize(2);
    }

    @Test
    void testGetReservationById() {
        // Given
        Long reservationId = 1L;
        Reservation expectedReservation = new Reservation(1L, DeliveryMode.DRIVE, LocalDate.now(), LocalTime.of(10, 00),
                LocalTime.of(12, 00), 1L);

        when(reservationRepository.findById(reservationId)).thenReturn(Optional.of(expectedReservation));

        // When
        Optional<Reservation> result = reservationService.getReservationById(reservationId);

        // Then
        assertThat(result).isPresent().contains(expectedReservation);
    }

    @Test
    void testSaveReservation() {
        // Given
        Reservation reservationToSave = new Reservation(1L, DeliveryMode.DRIVE, LocalDate.now(), LocalTime.of(10, 00),
                LocalTime.of(12, 00), 1L);
        when(reservationRepository.save(any(Reservation.class))).thenReturn(reservationToSave);

        // When
        Reservation savedReservation = reservationService.saveReservation(reservationToSave);

        // Then
        assertThat(savedReservation).isNotNull();
        assertThat(savedReservation.getId()).isEqualTo(1L);
        assertThat(savedReservation.getMode()).isEqualTo(DeliveryMode.DRIVE);
    }
}
