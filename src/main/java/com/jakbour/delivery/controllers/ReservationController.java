package com.jakbour.delivery.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jakbour.delivery.entities.Reservation;
import com.jakbour.delivery.service.ReservationService;

@RestController
@RequestMapping("/api/reservations")
public class ReservationController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<EntityModel<Reservation>>> getAllReservations() {
        List<Reservation> reservations = reservationService.getAllReservations();

        if (!reservations.isEmpty()) {
            List<EntityModel<Reservation>> reservationModels = reservations.stream()
                    .map(reservation -> EntityModel.of(reservation,
                            WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(ReservationController.class)
                                    .getReservationById(reservation.getId())).withSelfRel()))
                    .collect(Collectors.toList());

            CollectionModel<EntityModel<Reservation>> collectionModel = CollectionModel.of(reservationModels,
                    linkTo(methodOn(ReservationController.class).getAllReservations()).withSelfRel());

            return ResponseEntity.ok(collectionModel);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("/{reservationId}")
    public ResponseEntity<EntityModel<Reservation>> getReservationById(@PathVariable @NonNull Long reservationId) {
        Optional<Reservation> reservation = reservationService.getReservationById(reservationId);

        if (reservation.isPresent()) {
            EntityModel<Reservation> reservationModel = EntityModel.of(reservation.get(),
                    linkTo(methodOn(ReservationController.class)
                            .getReservationById(reservationId)).withSelfRel(),
                    linkTo(methodOn(ReservationController.class).getAllReservations()).withRel("reservations"));
            return ResponseEntity.ok(reservationModel);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @PostMapping
    public ResponseEntity<Void> createReservation(@RequestBody @NonNull Reservation reservation) {
        reservationService.saveReservation(reservation);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{reservationId}")
    public ResponseEntity<Void> updateReservation(@PathVariable @NonNull Long reservationId,
            @RequestBody Reservation reservation) {
        Optional<Reservation> existingReservation = reservationService.getReservationById(reservationId);
        if (existingReservation.isPresent()) {
            Reservation reservationValue = existingReservation.get();
            Optional.ofNullable(reservation.getMode()).ifPresent(reservationValue::setMode);
            Optional.ofNullable(reservation.getDate()).ifPresent(reservationValue::setDate);
            Optional.ofNullable(reservation.getTimeSlotStart()).ifPresent(reservationValue::setTimeSlotStart);
            Optional.ofNullable(reservation.getTimeSlotEnd()).ifPresent(reservationValue::setTimeSlotEnd);
            Optional.ofNullable(reservation.getUserId()).ifPresent(reservationValue::setUserId);

            reservationService.saveReservation(reservationValue);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{reservationId}")
    public ResponseEntity<Void> deleteReservation(@PathVariable @NonNull Long reservationId) {
        Optional<Reservation> reservation = reservationService.getReservationById(reservationId);
        if (reservation.isPresent()) {
            reservationService.deleteReservation(reservationId);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
