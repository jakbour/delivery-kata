package com.jakbour.delivery.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jakbour.delivery.entities.DeliveryMode;

@RestController
@RequestMapping("/api/DeliveryModes")
public class DeliveryModeController {
    

    @GetMapping
    public DeliveryMode[] getAllReservations() {
        return DeliveryMode.values();
    }

}
