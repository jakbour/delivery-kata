package com.jakbour.delivery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.jakbour.delivery.entities.Reservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long>{
    
}
