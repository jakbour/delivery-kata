package com.jakbour.delivery.service;

import java.util.List;
import java.util.Optional;

import org.springframework.lang.NonNull;
import com.jakbour.delivery.entities.Reservation;

public interface ReservationService {

    Optional<Reservation> getReservationById(@NonNull Long reservationId);

    List<Reservation> getAllReservations();

    Reservation saveReservation(@NonNull Reservation reservation);

    void deleteReservation(@NonNull Long reservationId);
}
