package com.jakbour.delivery.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import com.jakbour.delivery.entities.Reservation;
import com.jakbour.delivery.repository.ReservationRepository;

@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Optional<Reservation> getReservationById(@NonNull Long reservationId) {
        return reservationRepository.findById(reservationId);
    }

    @Override
    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    @Override
    public Reservation saveReservation(@NonNull Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    @Override
    public void deleteReservation(@NonNull Long reservationId) {
        reservationRepository.deleteById(reservationId);
    }

}
