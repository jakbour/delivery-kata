package com.jakbour.delivery.entities;

import java.time.LocalDate;
import java.time.LocalTime;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Reservation {
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private DeliveryMode mode;
    private LocalDate date;
    private LocalTime timeSlotStart;
    private LocalTime timeSlotEnd;
    private Long userId;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public DeliveryMode getMode() {
        return mode;
    }
    public void setMode(DeliveryMode mode) {
        this.mode = mode;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public LocalTime getTimeSlotStart() {
        return timeSlotStart;
    }
    public void setTimeSlotStart(LocalTime timeSlotStart) {
        this.timeSlotStart = timeSlotStart;
    }
    public LocalTime getTimeSlotEnd() {
        return timeSlotEnd;
    }
    public void setTimeSlotEnd(LocalTime timeSlotEnd) {
        this.timeSlotEnd = timeSlotEnd;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Reservation(Long id, DeliveryMode mode, LocalDate date, LocalTime timeSlotStart, LocalTime timeSlotEnd,
            Long userId) {
        this.id = id;
        this.mode = mode;
        this.date = date;
        this.timeSlotStart = timeSlotStart;
        this.timeSlotEnd = timeSlotEnd;
        this.userId = userId;
    }
    public Reservation() {
    }

}
