package com.jakbour.delivery.entities;

public enum DeliveryMode {
    DRIVE("DRIVE"),
    DELIVERY("DELIVERY"),
    DELIVERY_TODAY("DELIVERY_TODAY"),
    DELIVERY_ASAP("DELIVERY_ASAP");

    private final String mode;

    // Constructor to initialize the enum constant with a string value
    DeliveryMode(String mode) {
        this.mode = mode;
    }

    // Getter method to retrieve the string value
    public String getMode() {
        return mode;
    }

}

